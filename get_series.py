from pandas_datareader import data
import pandas as pd
from datetime import date


def main(stock=None, start_date=None, end_date=None):
    df = data.DataReader(
        stock, 
        start=start_date, end=end_date,
        data_source='yahoo'
    )
    df['Date'] = df.index
    df['Date'] = df['Date'].dt.strftime('%Y%m%d')
    df['Symbol'] = stock
    df = df[['Symbol','Date','Open','High','Low', 'Close','Volume']]
    return df


if __name__ == '__main__':
    today = '{}'.format(date.today())
    stocks = ['AAPL', 'ABT']
    for stock in stocks:
        try:
            serie = main(stock=stock, start_date='2000-1-1', end_date=today)
            serie.to_csv('DataBases/{}.txt'.format(stock), sep=',', header=False, index=False)
            print('{} - success'.format(stock))
        except Exception:
            print('{} - ERROR'.format(stock))